var express = require('express');
var app = express();
var http = require('http').Server(app);

var async = require('async');
var fs = require('fs');
var util = require('util');
var serveIndex = require('serve-index');
var _ = require('underscore');

var tools = require('./util/tools');
var gl = require('./globals');


app.use(express.static(__dirname + "/static"));
//app.use("/photo", serveIndex(gl.photo_folder));
//app.use("/photo", express.static(gl.photo_folder));
//app.use("/thumbs", express.static(gl.thumbs_folder));
//app.use("/pub", serveIndex(__dirname + "/static"));
//app.use("/files", express.static(__dirname + "/files"));


function handleError(res, err) {
    if (err.message != null)
        tools.send_data(res, { result: "error", info: err.message});
    else
        tools.send_data(res, { result: "error", info: err});
}


app.get('/hello', function (req, res) {
    tools.send_data(res, {hello: "Plusot.com API", now: tools.fileDateLong()});
});


app.get('*', function (req, res) {
    tools.send_data(res, { result: "error", info: "invalid url: " + req.path});
});


http.listen(gl.server_port);

console.log('Plusot.com started ' + tools.fileDateLong() + ' running on port: ' + gl.server_port);

//eof
