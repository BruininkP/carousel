// tools.js
// ========

var async = require('async'),
    fs = require('fs'),
    paths = require('path'),
    ext = require('./extensions');

var make_error = function (err, msg) {
    var e = new Error(msg);
    e.code = err;
    return e;
};

var checkPath = function(path) {
    if (!pathChecked[path]) try {
        pathChecked[path] = true;

        fs.mkdirSync(path);
    } catch(e) {
        if (e.code !== "EEXIST") console.log('Could not create path ' + path + ' (' + e.message + ')');
    }
};

var fileType = {NONE: 0, FILES: 1, DIRS: 2};
var pathChecked = {};

module.exports = {
    FILETYPE : fileType,

    copy : function (original)  {
        return JSON.parse(JSON.stringify( original ));
    },

    fileDate : function () {
        var d = new Date();
        var curr_day = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();
        return '' + curr_year + curr_month.pad(2) + curr_day.pad(2);

    },

    fileDateLong : function () {
        var d = new Date();
        //var d = new Date(t.getTime() - t.getTimezoneOffset() * 60000);
        var curr_day = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();
        var curr_hour = d.getHours();
        var curr_min = d.getMinutes();
        var curr_sec = d.getSeconds();

        return '' + curr_year + '-' + curr_month.pad(2) + '-' + curr_day.pad(2) + ' ' + curr_hour + ':' + curr_min.pad(2) + ':' + curr_sec.pad(2) ;

    },

    invalid_resource: function (path) {
        return make_error("invalid_resource", path);
    },

    invalid_json: function (json) {
        return make_error("invalid_json", json);
    },

    no_user_data: function (path) {
        return make_error("no_user_data", path);
    },

    no_session: function (data) {
        return make_error("no_session", data);
    },

    no_devices: function (data) {
        return make_error("no_devices_available", data);
    },

    send_success : function (res, data) {
        res.writeHead(200, {"Content-Type": "application/json"});
        var output = { ack: "ACK", data: data };
        res.end(JSON.stringify(output) + "\n");
    },

    send_successid : function (res, id) {
        res.writeHead(200, {"Content-Type": "application/json"});
        var output = { ack: "ACK", id: id };
        res.end(JSON.stringify(output) + "\n");
    },

    send_data : function (res, data, log) {
        if (log != null && log == true) console.log("Sending: " + JSON.stringify(data));
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(JSON.stringify(data) + "\n");
    },

    send_raw : function (res, data) {
        res.writeHead(200, {"Content-Type": "application/json"});
        res.end(data + "\n");
    },

    send_failure : function (res, intCode, err) {
        var code = (err.code) ? err.code : err.name;
        res.writeHead(intCode, { "Content-Type" : "application/json" });
        res.end(JSON.stringify({ error: code, message: err.message }) + "\n");
    },

    checkDirectory : function(directory, callback) {  
        fs.stat(directory, function(err, stats) {
            //Check if error defined and the error code is "not exists"
            if (err && (err.errno === 34 || err.errno === -2 || err.code === 'ENOENT')) {
                //Create the directory, call the callback.
                fs.mkdir(directory, callback);
            } else {
                //just in case there was a different error:
                callback(err)
            }
        });
    },

    find_files : function (path, fileTypes, callback) {
        fs.readdir(
            path,
            function (err, files) {
                if (err) {
                    callback(make_error("file_error", JSON.stringify(err)));
                    return;
                }

                var only_dirs = [];
                var only_files = { jpg :[], other: []};
                async.forEach(
                    files,
                    function (element, cb) {
                        var filePath = paths.join(path, element);
                        //                        console.log('find_files digging into ' + filePath + " for element " + element);
                        fs.stat(
                            filePath,
                            function (err, stats) {
                                if (err) {
                                    cb(make_error("file_error", JSON.stringify(err)));
                                    return;
                                }
                                if (stats.isDirectory() && !element.startsWith('.') && (fileTypes == null || (fileTypes & fileType.DIRS) > 0)) {
                                    only_dirs.push({ name: element });
                                } else if (stats.isFile() && (fileTypes == null || (fileTypes & fileType.FILES) > 0)) {
                                    var parts = element.split('.');

                                    if (parts.length > 1) {
                                        switch (parts[1].toLowerCase()) {
                                            case 'jpeg':
                                                only_files.jpg.push({ name : element});
                                                break;
                                            case 'jpg':
                                                only_files.jpg.push({ name : element});
                                                break;
                                            default:
                                                only_files.other.push({ name: element });
                                                break;
                                                                      } 
                                    } else
                                        only_files.other.push({ name: element });
                                }
                                cb(null);
                            }
                        );
                    },
                    function (err) {
                        if (fileTypes == null) callback(err, err ? null : { dirs: only_dirs, files: only_files});
                        else if ((fileTypes & fileType.DIRS) > 0) callback(err, err ? null : { dirs: only_dirs});
                        else if ((fileTypes & fileType.FILES) > 0) callback(err, err ? null : { files: only_files});
                    }
                );
            }
        );
    },


    save: function (path, fileName, jsonString, append) {
        var file = paths.join(path, fileName);
        checkPath(path);
        if (append) {
            jsonString += ',';
            fs.appendFile(file, jsonString, function (err) {
                if (err) throw err;
                //console.log(jsonString + ' is appended to ' + fileName + '!');
            })
        } else {
            fs.writeFile(file, jsonString, function (err) {
                if (err) throw err;
                //console.log(jsonString + ' is written to ' + fileName + '!');
            })
        }
    },

    time2Str : function (time) {
        var t = new Date(time);
        var str = t.getUTCHours() + ':';
        if (t.getUTCMinutes() < 10)
            str += '0' + t.getUTCMinutes() + ':';
        else
            str += t.getUTCMinutes() + ':';
        if (t.getUTCSeconds() < 10) return str + '0' + t.getUTCSeconds();
        return str + t.getUTCSeconds();
    },

    pad: function (value, format) {
        var l = format.length;
        var tmp = format + value;
        var t = tmp.length;
        return tmp.substring(t - l, t);
    }

};
