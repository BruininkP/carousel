/**
 * Created by peet on 7-11-14.
 */

var pad = function (value, len) {
    var num = String(value);
    if (num.length >= len) return num;
    return (Array(len).join("0") + num).slice(-len);
}

function init() {
    Number.prototype.pad = function (len) {
        return pad(this, len);
    }
    if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (str){
            return this.slice(0, str.length) == str;
        };
    }
}

exports.pad = pad;

init();
