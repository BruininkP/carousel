var widerLeft = null;
var widerRight = null;
var slideOutWidth = 101;
var rightClickTime = 0;
var leftClickTime = 0;
var imageWidthPlusMargin = 312;
var widerDelay = 400;

function getTxt() {
    $.ajax({
        url:'text.txt',
        success: function (data){
            //parse your data here
            //you can split into lines using data.split('\n')
            //an use regex functions to effectively parse it
        }
    });
}

function collapseLeft() {
    if (widerLeft == null) return
    var temp = widerLeft
    widerLeft.animate({'minWidth': '0'}, widerDelay, function() {
        temp.css({'display': 'none'})
        temp.sliderParent.css({'margin-left': '30px'})
    })
    widerLeft.slider.animate( { scrollLeft: '-=' + slideOutWidth }, widerDelay);
    widerLeft = null

}

function collapseRight() {
    if (widerRight == null) return
    var temp = widerRight
    widerRight.animate({'minWidth': '0'}, widerDelay, function() {
        temp.css({'display': 'none'})
        temp.sliderParent.css({'margin-right': '30px'})
    })
    widerRight = null;
}

function collapse() {
    collapseLeft()
    collapseRight()
}

function leftSlide(slider, parent, thisId, delay) {
    widerLeft = $('#' + thisId + 'Left');
    setTimeout(function() {
        if (widerLeft == null) return
        widerLeft.css({'display': 'inline-block'})
        widerLeft.animate({minWidth: slideOutWidth }, widerDelay)
        parent.css({'margin-left': '0px'})
        slider.animate( {scrollLeft: '+=' + slideOutWidth  }, widerDelay);
    }, delay);
    widerLeft.sliderParent = parent
    widerLeft.slider = slider
}

function rightSlide(slider, parent, thisId, delay) {
    widerRight = $('#' + thisId + 'Right');
    setTimeout(function() {
        if (widerRight == null) return
        widerRight.css({'display': 'inline-block'})
        widerRight.animate({'minWidth': slideOutWidth}, widerDelay)
        parent.css({'margin-right': '0px'})
    }, delay);
    widerRight.slider = slider
    widerRight.sliderParent = parent
}

function slideClick(slider, thisId, isLeft, hasLeft) {
    console.log("slideClick called")
    var sliderWidth = slider.offsetWidth;
    var parent = $('#' + thisId)
    var delayIt = false
    if (widerLeft != null && widerLeft[0] != null) {
        if (widerLeft[0].id.indexOf(thisId) >= 0) {
            if (isLeft) {
                collapseLeft()
                return
            }
        } else {
            delayIt = true
            collapseLeft()
        }
    }
    if (widerRight != null) {
        if (widerRight[0].id.indexOf(thisId) >= 0) {
            if (!isLeft) {
                collapseRight()
                return;
            }
        } else {
            delayIt = true
            collapseRight()
        }
    }
    var delay = 50;
    if (delayIt) delay += widerDelay

    if (isLeft) {
        if (hasLeft) leftSlide(slider, parent, thisId, delay)
    } else {
        rightSlide(slider, parent, thisId, delay)
    }
}


function init(content, sliderContainerId) {
    console.log("Init called")
    var sliderContainer = $("#" + sliderContainerId)
    sliderContainer.append(
        '<div id="' + sliderContainerId + 'Slider" class="slider"></div>' +
        '<div id="' + sliderContainerId + 'ArrowLeft" class="buttonSmall buttonLeft"><i class="fas fa-angle-left"></i></div>' +
        '<div id="' + sliderContainerId + 'ArrowRight" class="buttonSmall buttonRight"><i class="fas fa-angle-right"></i></div>'
    )
    var sliderId = sliderContainerId + 'Slider'
    var slider = $("#" + sliderId)

    slider.append('<div class="dummyElement"></div>')

    for (var i = 0; i < content.length; i++) {


        if (content[i].leftText != null) slider.append('<div id="' + sliderId +'Item' + (i + 1) + 'Left" class="itemPart left">' + content[i].leftText +'</div>')

        var leftHover = ""
        if (content[i].leftText != null) leftHover = '<div class="hoverButtonLeft"><i class="fas fa-ellipsis-v"></i></div>'
        slider.append(
            '<div id="' + sliderId +'Item' + (i + 1) + '" class="itemPart mid">' +
            '<img src="' + content[i].image + '"/>' +
            leftHover +
            '<div class="hoverButtonRight"><i class="fas fa-ellipsis-v"></i></div>' +
            '</div>' +
            '<div id="' + sliderId +'Item' + (i + 1) + 'Right" class="itemPart right">' + content[i].rightItems +'</div>'
        )

        var sliderItem = $('#' + sliderId +'Item' + (i + 1));
        sliderItem[0].hasLeft = content[i].leftText != null

        var hoverButtonLeft = $('#' + sliderId +'Item' + (i + 1) + " .hoverButtonLeft")
        if (hoverButtonLeft[0] != null) hoverButtonLeft[0].sliderItemId = sliderItem[0].id;
        var hoverButtonRight = $('#' + sliderId +'Item' + (i + 1) + " .hoverButtonRight")
        if (hoverButtonRight[0] != null) hoverButtonRight[0].sliderItemId = sliderItem[0].id;

        sliderItem.click( function(event) {
            slideClick(slider, this.id, event.originalEvent.offsetX < this.offsetWidth / 2, this.hasLeft)
        });

        hoverButtonRight.click( function(event) {
            event.stopPropagation();
            slideClick(slider, this.sliderItemId, false, false)
        })
    }

    slider.append('<div class="dummyElement"></div>')
    var delta = imageWidthPlusMargin + (content.length * imageWidthPlusMargin) / 2 - (window.innerWidth - 40) / 2
    slider.animate( { scrollLeft: '+=' + delta }, 0);

    var arrowLeft = $('#' + sliderContainerId + 'ArrowLeft');
    arrowLeft[0].slider = slider;
    arrowLeft.click(function() {
        collapse()
        if (this.slider.scrollLeft < imageWidthPlusMargin) return;
        var time = new Date().getTime();
        if (time - leftClickTime > 500) {
            this.slider.animate( { scrollLeft: '-=' + imageWidthPlusMargin }, widerDelay / 2); //{ duration: 500});
            //slider[0].scrollLeft += 100;
            leftClickTime = time
        } else {
            this.slider.animate( { scrollLeft: '-=' + imageWidthPlusMargin }, { duration: 2 * (time - leftClickTime) /3});
            leftClickTime = time;
        }
    })

    var arrowRight = $('#' + sliderContainerId + 'ArrowRight');
    arrowRight[0].slider = slider;
    arrowRight.click(function() {
        collapse()
        console.log((window.innerWidth - 40 + slider[0].scrollLeft) + ">" + (1 + content.length) * imageWidthPlusMargin)
        if (window.innerWidth - 40 + slider[0].scrollLeft > (1 + content.length) * imageWidthPlusMargin) return;
        var time = new Date().getTime();
        if (time - rightClickTime > 500) {
            this.slider.animate( { scrollLeft: '+=' + imageWidthPlusMargin }, { duration: widerDelay / 2} );
            rightClickTime = time;
        } else {
            this.slider.animate( { scrollLeft: '+=' + imageWidthPlusMargin }, { duration: 2 * (time - rightClickTime) /3});
            rightClickTime = time;
        }
    })

    $('#topnavbar').affix({
        offset: {
            top: $('#banner').height()
        }
    });

}
